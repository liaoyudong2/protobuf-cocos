import * as $protobuf from "../libraries/protobuf";
/** Properties of a MsgPackageHeader. */
export interface IMsgPackageHeader {

    /** MsgPackageHeader Type */
    Type?: (number|null);

    /** MsgPackageHeader MsgId */
    MsgId?: (number|null);
}

/** Represents a MsgPackageHeader. */
export class MsgPackageHeader implements IMsgPackageHeader {

    /**
     * Constructs a new MsgPackageHeader.
     * @param [properties] Properties to set
     */
    constructor(properties?: IMsgPackageHeader);

    /** MsgPackageHeader Type. */
    public Type: number;

    /** MsgPackageHeader MsgId. */
    public MsgId: number;

    /**
     * Creates a new MsgPackageHeader instance using the specified properties.
     * @param [properties] Properties to set
     * @returns MsgPackageHeader instance
     */
    public static create(properties?: IMsgPackageHeader): MsgPackageHeader;

    /**
     * Encodes the specified MsgPackageHeader message. Does not implicitly {@link MsgPackageHeader.verify|verify} messages.
     * @param message MsgPackageHeader message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IMsgPackageHeader, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified MsgPackageHeader message, length delimited. Does not implicitly {@link MsgPackageHeader.verify|verify} messages.
     * @param message MsgPackageHeader message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IMsgPackageHeader, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a MsgPackageHeader message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns MsgPackageHeader
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): MsgPackageHeader;

    /**
     * Decodes a MsgPackageHeader message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns MsgPackageHeader
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): MsgPackageHeader;

    /**
     * Verifies a MsgPackageHeader message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a MsgPackageHeader message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns MsgPackageHeader
     */
    public static fromObject(object: { [k: string]: any }): MsgPackageHeader;

    /**
     * Creates a plain object from a MsgPackageHeader message. Also converts values to other types if specified.
     * @param message MsgPackageHeader
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: MsgPackageHeader, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this MsgPackageHeader to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a MsgAccountLoginReq. */
export interface IMsgAccountLoginReq {

    /** MsgAccountLoginReq Plat */
    Plat?: (string|null);

    /** MsgAccountLoginReq Account */
    Account?: (string|null);

    /** MsgAccountLoginReq Password */
    Password?: (string|null);

    /** MsgAccountLoginReq ZoneId */
    ZoneId?: (number|null);
}

/** Represents a MsgAccountLoginReq. */
export class MsgAccountLoginReq implements IMsgAccountLoginReq {

    /**
     * Constructs a new MsgAccountLoginReq.
     * @param [properties] Properties to set
     */
    constructor(properties?: IMsgAccountLoginReq);

    /** MsgAccountLoginReq Plat. */
    public Plat: string;

    /** MsgAccountLoginReq Account. */
    public Account: string;

    /** MsgAccountLoginReq Password. */
    public Password: string;

    /** MsgAccountLoginReq ZoneId. */
    public ZoneId: number;

    /**
     * Creates a new MsgAccountLoginReq instance using the specified properties.
     * @param [properties] Properties to set
     * @returns MsgAccountLoginReq instance
     */
    public static create(properties?: IMsgAccountLoginReq): MsgAccountLoginReq;

    /**
     * Encodes the specified MsgAccountLoginReq message. Does not implicitly {@link MsgAccountLoginReq.verify|verify} messages.
     * @param message MsgAccountLoginReq message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IMsgAccountLoginReq, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified MsgAccountLoginReq message, length delimited. Does not implicitly {@link MsgAccountLoginReq.verify|verify} messages.
     * @param message MsgAccountLoginReq message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IMsgAccountLoginReq, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a MsgAccountLoginReq message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns MsgAccountLoginReq
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): MsgAccountLoginReq;

    /**
     * Decodes a MsgAccountLoginReq message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns MsgAccountLoginReq
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): MsgAccountLoginReq;

    /**
     * Verifies a MsgAccountLoginReq message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a MsgAccountLoginReq message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns MsgAccountLoginReq
     */
    public static fromObject(object: { [k: string]: any }): MsgAccountLoginReq;

    /**
     * Creates a plain object from a MsgAccountLoginReq message. Also converts values to other types if specified.
     * @param message MsgAccountLoginReq
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: MsgAccountLoginReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this MsgAccountLoginReq to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}

/** Properties of a MsgAccountLoginAck. */
export interface IMsgAccountLoginAck {

    /** MsgAccountLoginAck Result */
    Result?: (number|null);
}

/** Represents a MsgAccountLoginAck. */
export class MsgAccountLoginAck implements IMsgAccountLoginAck {

    /**
     * Constructs a new MsgAccountLoginAck.
     * @param [properties] Properties to set
     */
    constructor(properties?: IMsgAccountLoginAck);

    /** MsgAccountLoginAck Result. */
    public Result: number;

    /**
     * Creates a new MsgAccountLoginAck instance using the specified properties.
     * @param [properties] Properties to set
     * @returns MsgAccountLoginAck instance
     */
    public static create(properties?: IMsgAccountLoginAck): MsgAccountLoginAck;

    /**
     * Encodes the specified MsgAccountLoginAck message. Does not implicitly {@link MsgAccountLoginAck.verify|verify} messages.
     * @param message MsgAccountLoginAck message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(message: IMsgAccountLoginAck, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Encodes the specified MsgAccountLoginAck message, length delimited. Does not implicitly {@link MsgAccountLoginAck.verify|verify} messages.
     * @param message MsgAccountLoginAck message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(message: IMsgAccountLoginAck, writer?: $protobuf.Writer): $protobuf.Writer;

    /**
     * Decodes a MsgAccountLoginAck message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns MsgAccountLoginAck
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): MsgAccountLoginAck;

    /**
     * Decodes a MsgAccountLoginAck message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns MsgAccountLoginAck
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): MsgAccountLoginAck;

    /**
     * Verifies a MsgAccountLoginAck message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): (string|null);

    /**
     * Creates a MsgAccountLoginAck message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns MsgAccountLoginAck
     */
    public static fromObject(object: { [k: string]: any }): MsgAccountLoginAck;

    /**
     * Creates a plain object from a MsgAccountLoginAck message. Also converts values to other types if specified.
     * @param message MsgAccountLoginAck
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(message: MsgAccountLoginAck, options?: $protobuf.IConversionOptions): { [k: string]: any };

    /**
     * Converts this MsgAccountLoginAck to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
}
