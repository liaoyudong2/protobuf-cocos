import { ProtoCls } from "../framework/ProtoCls";
import {
	IMsgAccountLoginReq,
	IMsgAccountLoginAck,
} from "./proto";

export class MsgAccountLoginReqProto extends ProtoCls {
    constructor() { super(); }
    public encode(obj: IMsgAccountLoginReq): Uint8Array {
        return this._protobuf.roots.default.MsgAccountLoginReq.encode(this._protobuf.roots.default.MsgAccountLoginReq.create(obj)).finish();
    }
    public decode(message: Uint8Array): IMsgAccountLoginReq {
        return this._protobuf.roots.default.MsgAccountLoginReq.decode(message);
    }
}

export class MsgAccountLoginAckProto extends ProtoCls {
    constructor() { super(); }
    public encode(obj: IMsgAccountLoginAck): Uint8Array {
        return this._protobuf.roots.default.MsgAccountLoginAck.encode(this._protobuf.roots.default.MsgAccountLoginAck.create(obj)).finish();
    }
    public decode(message: Uint8Array): IMsgAccountLoginAck {
        return this._protobuf.roots.default.MsgAccountLoginAck.decode(message);
    }
}
