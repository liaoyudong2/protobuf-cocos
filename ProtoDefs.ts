import {
	MsgAccountLoginReqProto,
	MsgAccountLoginAckProto,
} from "./Proto_login";

export class ProtoDefs {
    private static _ids: { [key: number]: any } = {};

    // MSGID //
	/** 101: 用户账户登录请求 */
	public static ID_MsgAccountLoginReq: number = 101;
	/** 102: 用户账户登录返回 */
	public static ID_MsgAccountLoginAck: number = 102;
    // MSGID //

    public static register() {
		this._ids[this.ID_MsgAccountLoginReq] = MsgAccountLoginReqProto;
		this._ids[this.ID_MsgAccountLoginAck] = MsgAccountLoginAckProto;
    }

    public static getClass(msgId: number) {
        return this._ids[msgId];
    }
}