import { ProtoCls } from "../framework/ProtoCls";
import {
	IMsgPackageHeader,
} from "./proto";

export class MsgPackageHeaderProto extends ProtoCls {
    constructor() { super(); }
    public encode(obj: IMsgPackageHeader): Uint8Array {
        return this._protobuf.roots.default.MsgPackageHeader.encode(this._protobuf.roots.default.MsgPackageHeader.create(obj)).finish();
    }
    public decode(message: Uint8Array): IMsgPackageHeader {
        return this._protobuf.roots.default.MsgPackageHeader.decode(message);
    }
}
