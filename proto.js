/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = protobuf;

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.MsgPackageHeader = (function() {

    /**
     * Properties of a MsgPackageHeader.
     * @exports IMsgPackageHeader
     * @interface IMsgPackageHeader
     * @property {number|null} [Type] MsgPackageHeader Type
     * @property {number|null} [MsgId] MsgPackageHeader MsgId
     */

    /**
     * Constructs a new MsgPackageHeader.
     * @exports MsgPackageHeader
     * @classdesc Represents a MsgPackageHeader.
     * @implements IMsgPackageHeader
     * @constructor
     * @param {IMsgPackageHeader=} [properties] Properties to set
     */
    function MsgPackageHeader(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * MsgPackageHeader Type.
     * @member {number} Type
     * @memberof MsgPackageHeader
     * @instance
     */
    MsgPackageHeader.prototype.Type = 0;

    /**
     * MsgPackageHeader MsgId.
     * @member {number} MsgId
     * @memberof MsgPackageHeader
     * @instance
     */
    MsgPackageHeader.prototype.MsgId = 0;

    /**
     * Creates a new MsgPackageHeader instance using the specified properties.
     * @function create
     * @memberof MsgPackageHeader
     * @static
     * @param {IMsgPackageHeader=} [properties] Properties to set
     * @returns {MsgPackageHeader} MsgPackageHeader instance
     */
    MsgPackageHeader.create = function create(properties) {
        return new MsgPackageHeader(properties);
    };

    /**
     * Encodes the specified MsgPackageHeader message. Does not implicitly {@link MsgPackageHeader.verify|verify} messages.
     * @function encode
     * @memberof MsgPackageHeader
     * @static
     * @param {IMsgPackageHeader} message MsgPackageHeader message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    MsgPackageHeader.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.Type != null && Object.hasOwnProperty.call(message, "Type"))
            writer.uint32(/* id 1, wireType 0 =*/8).sint32(message.Type);
        if (message.MsgId != null && Object.hasOwnProperty.call(message, "MsgId"))
            writer.uint32(/* id 2, wireType 0 =*/16).sint32(message.MsgId);
        return writer;
    };

    /**
     * Encodes the specified MsgPackageHeader message, length delimited. Does not implicitly {@link MsgPackageHeader.verify|verify} messages.
     * @function encodeDelimited
     * @memberof MsgPackageHeader
     * @static
     * @param {IMsgPackageHeader} message MsgPackageHeader message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    MsgPackageHeader.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a MsgPackageHeader message from the specified reader or buffer.
     * @function decode
     * @memberof MsgPackageHeader
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {MsgPackageHeader} MsgPackageHeader
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    MsgPackageHeader.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.MsgPackageHeader();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.Type = reader.sint32();
                break;
            case 2:
                message.MsgId = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a MsgPackageHeader message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof MsgPackageHeader
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {MsgPackageHeader} MsgPackageHeader
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    MsgPackageHeader.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a MsgPackageHeader message.
     * @function verify
     * @memberof MsgPackageHeader
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    MsgPackageHeader.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.Type != null && message.hasOwnProperty("Type"))
            if (!$util.isInteger(message.Type))
                return "Type: integer expected";
        if (message.MsgId != null && message.hasOwnProperty("MsgId"))
            if (!$util.isInteger(message.MsgId))
                return "MsgId: integer expected";
        return null;
    };

    /**
     * Creates a MsgPackageHeader message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof MsgPackageHeader
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {MsgPackageHeader} MsgPackageHeader
     */
    MsgPackageHeader.fromObject = function fromObject(object) {
        if (object instanceof $root.MsgPackageHeader)
            return object;
        var message = new $root.MsgPackageHeader();
        if (object.Type != null)
            message.Type = object.Type | 0;
        if (object.MsgId != null)
            message.MsgId = object.MsgId | 0;
        return message;
    };

    /**
     * Creates a plain object from a MsgPackageHeader message. Also converts values to other types if specified.
     * @function toObject
     * @memberof MsgPackageHeader
     * @static
     * @param {MsgPackageHeader} message MsgPackageHeader
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    MsgPackageHeader.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.defaults) {
            object.Type = 0;
            object.MsgId = 0;
        }
        if (message.Type != null && message.hasOwnProperty("Type"))
            object.Type = message.Type;
        if (message.MsgId != null && message.hasOwnProperty("MsgId"))
            object.MsgId = message.MsgId;
        return object;
    };

    /**
     * Converts this MsgPackageHeader to JSON.
     * @function toJSON
     * @memberof MsgPackageHeader
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    MsgPackageHeader.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return MsgPackageHeader;
})();

$root.MsgAccountLoginReq = (function() {

    /**
     * Properties of a MsgAccountLoginReq.
     * @exports IMsgAccountLoginReq
     * @interface IMsgAccountLoginReq
     * @property {string|null} [Plat] MsgAccountLoginReq Plat
     * @property {string|null} [Account] MsgAccountLoginReq Account
     * @property {string|null} [Password] MsgAccountLoginReq Password
     * @property {number|null} [ZoneId] MsgAccountLoginReq ZoneId
     */

    /**
     * Constructs a new MsgAccountLoginReq.
     * @exports MsgAccountLoginReq
     * @classdesc Represents a MsgAccountLoginReq.
     * @implements IMsgAccountLoginReq
     * @constructor
     * @param {IMsgAccountLoginReq=} [properties] Properties to set
     */
    function MsgAccountLoginReq(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * MsgAccountLoginReq Plat.
     * @member {string} Plat
     * @memberof MsgAccountLoginReq
     * @instance
     */
    MsgAccountLoginReq.prototype.Plat = "";

    /**
     * MsgAccountLoginReq Account.
     * @member {string} Account
     * @memberof MsgAccountLoginReq
     * @instance
     */
    MsgAccountLoginReq.prototype.Account = "";

    /**
     * MsgAccountLoginReq Password.
     * @member {string} Password
     * @memberof MsgAccountLoginReq
     * @instance
     */
    MsgAccountLoginReq.prototype.Password = "";

    /**
     * MsgAccountLoginReq ZoneId.
     * @member {number} ZoneId
     * @memberof MsgAccountLoginReq
     * @instance
     */
    MsgAccountLoginReq.prototype.ZoneId = 0;

    /**
     * Creates a new MsgAccountLoginReq instance using the specified properties.
     * @function create
     * @memberof MsgAccountLoginReq
     * @static
     * @param {IMsgAccountLoginReq=} [properties] Properties to set
     * @returns {MsgAccountLoginReq} MsgAccountLoginReq instance
     */
    MsgAccountLoginReq.create = function create(properties) {
        return new MsgAccountLoginReq(properties);
    };

    /**
     * Encodes the specified MsgAccountLoginReq message. Does not implicitly {@link MsgAccountLoginReq.verify|verify} messages.
     * @function encode
     * @memberof MsgAccountLoginReq
     * @static
     * @param {IMsgAccountLoginReq} message MsgAccountLoginReq message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    MsgAccountLoginReq.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.Plat != null && Object.hasOwnProperty.call(message, "Plat"))
            writer.uint32(/* id 1, wireType 2 =*/10).string(message.Plat);
        if (message.Account != null && Object.hasOwnProperty.call(message, "Account"))
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.Account);
        if (message.Password != null && Object.hasOwnProperty.call(message, "Password"))
            writer.uint32(/* id 3, wireType 2 =*/26).string(message.Password);
        if (message.ZoneId != null && Object.hasOwnProperty.call(message, "ZoneId"))
            writer.uint32(/* id 4, wireType 0 =*/32).sint32(message.ZoneId);
        return writer;
    };

    /**
     * Encodes the specified MsgAccountLoginReq message, length delimited. Does not implicitly {@link MsgAccountLoginReq.verify|verify} messages.
     * @function encodeDelimited
     * @memberof MsgAccountLoginReq
     * @static
     * @param {IMsgAccountLoginReq} message MsgAccountLoginReq message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    MsgAccountLoginReq.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a MsgAccountLoginReq message from the specified reader or buffer.
     * @function decode
     * @memberof MsgAccountLoginReq
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {MsgAccountLoginReq} MsgAccountLoginReq
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    MsgAccountLoginReq.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.MsgAccountLoginReq();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.Plat = reader.string();
                break;
            case 2:
                message.Account = reader.string();
                break;
            case 3:
                message.Password = reader.string();
                break;
            case 4:
                message.ZoneId = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a MsgAccountLoginReq message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof MsgAccountLoginReq
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {MsgAccountLoginReq} MsgAccountLoginReq
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    MsgAccountLoginReq.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a MsgAccountLoginReq message.
     * @function verify
     * @memberof MsgAccountLoginReq
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    MsgAccountLoginReq.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.Plat != null && message.hasOwnProperty("Plat"))
            if (!$util.isString(message.Plat))
                return "Plat: string expected";
        if (message.Account != null && message.hasOwnProperty("Account"))
            if (!$util.isString(message.Account))
                return "Account: string expected";
        if (message.Password != null && message.hasOwnProperty("Password"))
            if (!$util.isString(message.Password))
                return "Password: string expected";
        if (message.ZoneId != null && message.hasOwnProperty("ZoneId"))
            if (!$util.isInteger(message.ZoneId))
                return "ZoneId: integer expected";
        return null;
    };

    /**
     * Creates a MsgAccountLoginReq message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof MsgAccountLoginReq
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {MsgAccountLoginReq} MsgAccountLoginReq
     */
    MsgAccountLoginReq.fromObject = function fromObject(object) {
        if (object instanceof $root.MsgAccountLoginReq)
            return object;
        var message = new $root.MsgAccountLoginReq();
        if (object.Plat != null)
            message.Plat = String(object.Plat);
        if (object.Account != null)
            message.Account = String(object.Account);
        if (object.Password != null)
            message.Password = String(object.Password);
        if (object.ZoneId != null)
            message.ZoneId = object.ZoneId | 0;
        return message;
    };

    /**
     * Creates a plain object from a MsgAccountLoginReq message. Also converts values to other types if specified.
     * @function toObject
     * @memberof MsgAccountLoginReq
     * @static
     * @param {MsgAccountLoginReq} message MsgAccountLoginReq
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    MsgAccountLoginReq.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.defaults) {
            object.Plat = "";
            object.Account = "";
            object.Password = "";
            object.ZoneId = 0;
        }
        if (message.Plat != null && message.hasOwnProperty("Plat"))
            object.Plat = message.Plat;
        if (message.Account != null && message.hasOwnProperty("Account"))
            object.Account = message.Account;
        if (message.Password != null && message.hasOwnProperty("Password"))
            object.Password = message.Password;
        if (message.ZoneId != null && message.hasOwnProperty("ZoneId"))
            object.ZoneId = message.ZoneId;
        return object;
    };

    /**
     * Converts this MsgAccountLoginReq to JSON.
     * @function toJSON
     * @memberof MsgAccountLoginReq
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    MsgAccountLoginReq.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return MsgAccountLoginReq;
})();

$root.MsgAccountLoginAck = (function() {

    /**
     * Properties of a MsgAccountLoginAck.
     * @exports IMsgAccountLoginAck
     * @interface IMsgAccountLoginAck
     * @property {number|null} [Result] MsgAccountLoginAck Result
     */

    /**
     * Constructs a new MsgAccountLoginAck.
     * @exports MsgAccountLoginAck
     * @classdesc Represents a MsgAccountLoginAck.
     * @implements IMsgAccountLoginAck
     * @constructor
     * @param {IMsgAccountLoginAck=} [properties] Properties to set
     */
    function MsgAccountLoginAck(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * MsgAccountLoginAck Result.
     * @member {number} Result
     * @memberof MsgAccountLoginAck
     * @instance
     */
    MsgAccountLoginAck.prototype.Result = 0;

    /**
     * Creates a new MsgAccountLoginAck instance using the specified properties.
     * @function create
     * @memberof MsgAccountLoginAck
     * @static
     * @param {IMsgAccountLoginAck=} [properties] Properties to set
     * @returns {MsgAccountLoginAck} MsgAccountLoginAck instance
     */
    MsgAccountLoginAck.create = function create(properties) {
        return new MsgAccountLoginAck(properties);
    };

    /**
     * Encodes the specified MsgAccountLoginAck message. Does not implicitly {@link MsgAccountLoginAck.verify|verify} messages.
     * @function encode
     * @memberof MsgAccountLoginAck
     * @static
     * @param {IMsgAccountLoginAck} message MsgAccountLoginAck message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    MsgAccountLoginAck.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.Result != null && Object.hasOwnProperty.call(message, "Result"))
            writer.uint32(/* id 1, wireType 0 =*/8).sint32(message.Result);
        return writer;
    };

    /**
     * Encodes the specified MsgAccountLoginAck message, length delimited. Does not implicitly {@link MsgAccountLoginAck.verify|verify} messages.
     * @function encodeDelimited
     * @memberof MsgAccountLoginAck
     * @static
     * @param {IMsgAccountLoginAck} message MsgAccountLoginAck message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    MsgAccountLoginAck.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a MsgAccountLoginAck message from the specified reader or buffer.
     * @function decode
     * @memberof MsgAccountLoginAck
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {MsgAccountLoginAck} MsgAccountLoginAck
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    MsgAccountLoginAck.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.MsgAccountLoginAck();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.Result = reader.sint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a MsgAccountLoginAck message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof MsgAccountLoginAck
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {MsgAccountLoginAck} MsgAccountLoginAck
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    MsgAccountLoginAck.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a MsgAccountLoginAck message.
     * @function verify
     * @memberof MsgAccountLoginAck
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    MsgAccountLoginAck.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.Result != null && message.hasOwnProperty("Result"))
            if (!$util.isInteger(message.Result))
                return "Result: integer expected";
        return null;
    };

    /**
     * Creates a MsgAccountLoginAck message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof MsgAccountLoginAck
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {MsgAccountLoginAck} MsgAccountLoginAck
     */
    MsgAccountLoginAck.fromObject = function fromObject(object) {
        if (object instanceof $root.MsgAccountLoginAck)
            return object;
        var message = new $root.MsgAccountLoginAck();
        if (object.Result != null)
            message.Result = object.Result | 0;
        return message;
    };

    /**
     * Creates a plain object from a MsgAccountLoginAck message. Also converts values to other types if specified.
     * @function toObject
     * @memberof MsgAccountLoginAck
     * @static
     * @param {MsgAccountLoginAck} message MsgAccountLoginAck
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    MsgAccountLoginAck.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.defaults)
            object.Result = 0;
        if (message.Result != null && message.hasOwnProperty("Result"))
            object.Result = message.Result;
        return object;
    };

    /**
     * Converts this MsgAccountLoginAck to JSON.
     * @function toJSON
     * @memberof MsgAccountLoginAck
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    MsgAccountLoginAck.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return MsgAccountLoginAck;
})();

module.exports = $root;
